

# Background

![K8s Deployment](/doc/k8sdeploy.png)

Provides the declarative YAML files used to deploy an end-to-end web voting application to a Kubernetes cluster. The deployment of the web voting application is used to demonstrate the following K8s resources:

* Nginx Ingress Controller

* Deployments

* Services

* Pods

* StatefulSet-mongo-DB

* Persistent Volumes

* Persistent Volume Claims

* Network Policies

* HPA
